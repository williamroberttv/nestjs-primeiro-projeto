import { Controller, Get, Param, Post, Body, Put } from '@nestjs/common';
import { User } from './User';
import { UserService } from './user.service';

@Controller('user')
export class UserController {
  constructor(private userService: UserService) {}

  @Get()
  findAll() {
    return this.userService.findAll();
  }

  @Get(':name')
  findByName(@Param() params: { name: string }) {
    return this.userService.findByName(params.name);
  }

  @Post()
  create(@Body() body: User) {
    console.log(body);
    return this.userService.create(body);
  }

  @Put(':id')
  update(@Param() params: { id: string }, @Body() body: User) {
    this.userService.update(body, +params.id);
  }
}
