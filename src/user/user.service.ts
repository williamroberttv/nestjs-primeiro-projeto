import { Injectable } from '@nestjs/common';
import { User } from './User';

@Injectable()
export class UserService {
  public users: User[] = [
    { id: 1, name: 'William', age: 25 },
    { id: 2, name: 'Robert', age: 25 },
    { id: 3, name: 'Tavares', age: 25 },
  ];

  findAll() {
    return this.users;
  }

  findByName(name: string) {
    console.log(name);
    const filteredUser = this.users.find((user) => user.name === name);
    return filteredUser;
  }

  create(user: User) {
    const id = this.users.length + 1;
    return this.users.push({ id, ...user });
  }

  update(user: User, id: number) {
    const userIndex = this.users.findIndex((user) => user.id === id);
    console.log(typeof userIndex);
    if (userIndex < 0) {
      return 'user not found';
    }
    return (this.users[userIndex] = { id, ...user });
  }
}
